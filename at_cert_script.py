#!/usr/bin/python
# pml
import os
from os import listdir
from os.path import isfile, join


MEMORYLOCATION = "ICACHE_RODATA_ATTR STORE_ATTR"

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

# Processing and write to at_certs.h
def atWrite(filename, arrayDefaultName, arrayNewName, defineName):
	file_at_certs = open("output/at_certs.h", "a+")
	file_from_command = open(filename, 'r')
	lineStart = 0;
	lineEnd = 0;	
	if filename != "_temp/default_cli_key.c":
		# Define writing
		lineCounter = 0;		
		for line in file_from_command:
			lineCounter+=1;
			if arrayDefaultName in line:
				lineStart = lineCounter;
				# print line.replace(arrayDefaultName, arrayNewName)
				# print find_between(line, '[', ']')
				file_at_certs.write(''.join(["\n#define ", defineName, " ", find_between(line, '[', ']')]))
				arrayNewNameLen = ''.join(['\nunsigned int ', arrayNewName, '_len = '])
				file_at_certs.write(''.join([arrayNewNameLen, find_between(line, '[', ']'), ';\n']))			
				# print "lineStart = ", lineStart		
			if "};" in line and lineStart != 0:
				lineEnd = lineCounter;
				# print "lineEnd = ", lineEnd
				break
		file_from_command = open(filename, 'r')	
		# Array writing
		lineCounter = 0; #Reset counter
		for line in file_from_command:
			lineCounter+=1
			if lineCounter == lineStart:
				_tempLine = line.replace(''.join(['unsigned char ',arrayDefaultName]), ''.join(['static const char ', arrayNewName]))
				file_at_certs.write(_tempLine.replace('={',' ICACHE_RODATA_ATTR STORE_ATTR ={'))
			if (lineCounter <= lineEnd) and (lineCounter > lineStart):
				file_at_certs.write(line)
		# print "End line: " ,lineCounter
	else: #default_cli_key.c
		# Define writing
		# print "Copied default_cli_key"
		lineCounter = 0;		
		lineStart = 1;
		for line in file_from_command:
			lineCounter+=1;			
			if "};" in line and lineStart != 0:
				lineEnd = lineCounter;
				# print "lineEnd = ", lineEnd
			if "_len" in line:
				file_at_certs.write(''.join(["\n#define ", defineName, " ", find_between(line, '= ', ';')]))
				arrayNewNameLen = ''.join(['\nunsigned int ', arrayNewName, '_len = '])
				file_at_certs.write(''.join([arrayNewNameLen, find_between(line, '= ', ';'), ';\n']))			
				# print "lineStart = ", lineStart		
				break
		file_from_command = open(filename, 'r')	
		# Array writing
		lineCounter = 0; #Reset counter
		for line in file_from_command:
			lineCounter+=1
			if lineCounter == lineStart:
				_tempLine = line.replace(''.join(['unsigned char ',arrayDefaultName]), ''.join(['static const char ', arrayNewName]))
				file_at_certs.write(_tempLine.replace('= {',' ICACHE_RODATA_ATTR STORE_ATTR ={'))
			if (lineCounter <= lineEnd) and (lineCounter > lineStart):
				file_at_certs.write(line)
		# print "	End line: " ,lineCounter

	#Close all file
	file_at_certs.close()
	file_from_command.close()


# Check foler before processing
if not os.path.exists("_temp"):
    os.makedirs("_temp")
if not os.path.exists("output"):
    os.makedirs("output")

# Delete file before append
if(os.path.exists("_temp/default_cas_certificate.c")):
	os.remove("_temp/default_cas_certificate.c")
if(os.path.exists("_temp/default_cli_certificate.c")):
	os.remove("_temp/default_cli_certificate.c")
if(os.path.exists("_temp/default_cli_key.c")):
	os.remove("_temp/default_cli_key.c")
if(os.path.exists("output/at_certs.h")):
	os.remove("output/at_certs.h")

#Validate user input file
numOfCertfile = [0,0,0]
currentWorkingDir = os.getcwd()
listfileCwd = os.listdir(currentWorkingDir)

for fileCert in listfileCwd:
	if "ca.pem" in fileCert:
		numOfCertfile[0] +=1
		File_AT_CA_PEM = fileCert
	if "pem.crt" in fileCert:
		numOfCertfile[1] +=1
		File_CERTIFICATE_PEM_CRT = fileCert
	if "pem.key" in fileCert:
		numOfCertfile[2] +=1
		File_KEY = fileCert


if numOfCertfile[0] != 1:
	print "Error: Too many at_ca.pem file in current folder! Found ", numOfCertfile[0]
if numOfCertfile[1] != 1:
	print "Error: Too many certificate.pem.crt file in current folder! Found ", numOfCertfile[1]
if numOfCertfile[2] != 1:
	print "Error: Too many .pem.key file in current folder! Found ", numOfCertfile[2]

if numOfCertfile[0] == 1 and numOfCertfile[1] == 1 and numOfCertfile[2] == 1:

	# list all allowed fiels
	print "\x1b[6;30;42m" + "Files found: \x1b[0m"
	print "	",File_AT_CA_PEM
	print "	", File_CERTIFICATE_PEM_CRT
	print "	", File_KEY
	# 1. Gen at_ca --> Copy the selected array to the default_cas_certificate[] in the at_certs.h
	# Note the define CA_LEN
	os.system(''.join(["openssl x509 -noout -in ", File_AT_CA_PEM, " -C>>_temp/default_cas_certificate.c"]))

	# 2. Convert certificate to array --> Copy the certification array to default_cli_certificate[] in at_certs.h
	# Note the define CERT_LEN is the same the array len
	os.system(''.join(["openssl x509 -noout -in ", File_CERTIFICATE_PEM_CRT, " -C>>_temp/default_cli_certificate.c"]))

	# 3. Convert RSA key to Der format
	# Copy the array to array default_cli_key[] in the at_certs.h
	os.system(''.join(["openssl rsa -in ", File_KEY," -inform PEM -out outputkey.der -outform DER"]))
	os.system("xxd -i outputkey.der>>_temp/default_cli_key.c")

	file_at = open("output/at_certs.h", "w")
	file_at.write("// RSA CA Certificate - AWS account: bklogy")
	file_at.close()

	print "STEP 1: Generate default_cas_certificate"
	atWrite("_temp/default_cas_certificate.c", "XXX_certificate", "default_cas_certificate", "CA_LEN")
	print "STEP 2: Generate default_cli_certificate"
	atWrite("_temp/default_cli_certificate.c", "XXX_certificate", "default_cli_certificate", "CERT_LEN")
	print "STEP 3: Generate default_cli_key"
	atWrite("_temp/default_cli_key.c", "outputkey_der", "default_cli_key", "KEY_LEN")
	print "\x1b[6;30;42m Generate at_certs.h successfully! To see it, please navigate to location: \output\x1b[0m"
else:
	print "\x1b[0;31;40m Generate at_certs.h failed!\x1b[0m"





